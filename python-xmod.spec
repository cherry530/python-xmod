%global _empty_manifest_terminate_build 0
Name:		python-xmod
Version:	1.8.1
Release:	1
Summary:	Create and recursively fill a temporary directory
License:	MIT
URL:		https://github.com/rec/xmod
Source0:	https://files.pythonhosted.org/packages/source/x/xmod/xmod-%{version}.tar.gz
BuildArch:	noarch

BuildRequires:  python3-pip  python3-hatchling python3-hatch-vcs python3-wheel
BuildRequires:  python3-poetry-core

%description
``xmod`` is a tiny library that extends a module to do things that normally
only a class could do - handy for modules that "just do one thing".

%package -n python3-xmod
Summary:	Create and recursively fill a temporary directory
Provides:	python-xmod
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-xmod
``xmod`` is a tiny library that extends a module to do things that normally
only a class could do - handy for modules that "just do one thing".

%package help
Summary:	Development documents and examples for xmod
Provides:	python3-xmod-doc
%description help
``xmod`` is a tiny library that extends a module to do things that normally
only a class could do - handy for modules that "just do one thing".

%prep
%autosetup -n xmod-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi

%files -n python3-xmod
%{python3_sitelib}/xmod*


%files help
%{_docdir}/*

%changelog
* Tue Mar 05 2024 xu_ping <707078654@qq.com> - 1.8.1-1
- Update to 1.8.1

* Tue Apr 25 2023 xu_ping <707078654@qq.com> - 1.4.0-1
- Update to 1.4.0

* Fri Aug 5 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 1.3.2-1
- Update to 1.3.2

* Tue Mar 1 2022 zhangy <zhangy1317@foxmai.com>
- Fix install require

* Thu Dec 31 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
